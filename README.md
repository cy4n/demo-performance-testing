# Continuous Performance Testing

this demo will show how you can leverage the Gatling Load Generator in continuous integration
planned: 
* Gatling basic scenarios and simulations
* run in every pipeline, with different KPIs per type (main branch vs. pullrequests)
* export metrics from gatling into grafana cloud for visualization (and evaluating data between multiple perftests)
